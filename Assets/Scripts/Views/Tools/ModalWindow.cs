﻿using System;
using AnimatorProject.Controllers.Actor;
using UnityEngine;
using UnityEngine.UI;

namespace AnimatorProject.Views.Tools
{
    public class ModalWindow : MonoBehaviour, IMachineState
    {
        [SerializeField] private Text messageText;
        [SerializeField] private Text triggerText;
        [SerializeField] private Button triggerButton;
        [SerializeField] private Image backgroundImage;

        private void Awake()
        {
            Deactivate();
        }

        public void Activate()
        {
            SetUiState(true);
        }

        public void Deactivate()
        {
            SetUiState(false);
        }

        private void SetUiState(bool state)
        {
            backgroundImage.enabled = state;
            triggerButton.enabled = state;
            triggerButton.image.enabled = state;
            messageText.enabled = state;
            triggerText.enabled = state;
        }     

        private void OnDestroy()
        {
            triggerButton.onClick.RemoveAllListeners();
        }

        public void Init(Action action)
        {
            triggerButton.onClick.AddListener(action.Invoke);
        }
    }
}

﻿using AnimatorProject.Controllers;
using AnimatorProject.Managers;
using AnimatorProject.Views.Tools;
using UnityEngine;

namespace AnimatorProject.Views
{
    [RequireComponent(typeof(ActorView))]
    public class UiManager : MonoBehaviour
    {
        [SerializeField] private ModalWindow endGameModalWindow;
        [HideInInspector][SerializeField] private ActorView actorView;
        
        private GameManager gameManager;
        private ActorController actorController;

        private void OnValidate()
        {
            actorView = GetComponent<ActorView>();
        }

        private void Start()
        {
            //get the actor controller
            gameManager = GameManager.instance;
            if (gameManager.actorManager.ActorController)
            {
                RegisterActorViews(gameManager.actorManager.ActorController);
            }
            else
            {
                gameManager.actorManager.OnActorRegistered += RegisterActorViews;
            }
        }

        public void RegisterActorViews(ActorController actorController)
        {
            this.actorController = actorController;

            actorView.InitHpValue(actorController.HealthController.MaxHp);
            actorController.HealthController.OnChangeHealth += actorView.SetHpValue;
            actorController.HealthController.OnDied += actorView.ZeroHpValue;

            gameManager.actorManager.OnActorDestroy += UnregisterActorViews;
        }

        public void UnregisterActorViews()
        {
            actorController.HealthController.OnChangeHealth -= actorView.SetHpValue;
            actorController.HealthController.OnDied -= actorView.ZeroHpValue;
            endGameModalWindow.Init(gameManager.levelManager.RestartLevel);
            endGameModalWindow.Activate();
        }
    }
}

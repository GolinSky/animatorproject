﻿using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace AnimatorProject.Views
{
    public class ActorView : MonoBehaviour
    {
        [SerializeField] private Image hpImage;
        private float coefficient;

        public void InitHpValue(float maxValue)
        {
            if (MathUtils.IsEqualZero(maxValue))
            {
                Debug.LogError($"Detected a possible zero-divide exception. Setting 1 to maxValue params-> [{name}, ActorView]");
                maxValue = 1;
            }
            coefficient = 1 / maxValue;
        }

        public void SetHpValue(float value)
        {
            hpImage.fillAmount = value * coefficient;
        }

        public void ZeroHpValue()
        {
            hpImage.fillAmount = 0;
        }
    }
}

﻿using AnimatorProject.Controllers;
using AnimatorProject.Controllers.Actor;
using UnityEngine;
using Utils;

namespace AnimatorProject.Utils
{
    public class InputController : BaseController, IMachineState
    {      
        private Vector2 keyBoardDirection = new Vector2();
        private Vector2 mouseDirection = new Vector2();
        private bool allowInput;
        private bool AllowInput => allowInput;

        public Vector2 KeyBoardDirection
        {
            get
            {
                keyBoardDirection.x = Input.GetAxis(InputParams.Horizontal);
                keyBoardDirection.y = Input.GetAxis(InputParams.Vertical);
                return keyBoardDirection;
            }
        }

        public Vector2 MouseDirection
        {
            get
            {
                mouseDirection.x = Input.GetAxis(InputParams.MouseX);
                mouseDirection.y = Input.GetAxis(InputParams.MouseY);
                return mouseDirection;
            }
        }

        public bool IsPressed => MathUtils.NotZero(KeyBoardDirection);

        public void Activate()
        {
            allowInput = true;
        }

        public void Deactivate()
        {
            allowInput = false;
        }

        protected override void AddListeners()
        {
            
        }

        protected override void RemoveListeners()
        {

        }
    }
}

﻿using UnityEngine;

namespace AnimatorProject.Utils
{
    public static class AnimatorLayersController
    {
        public static float GetLayerWeight(this Animator animator, AnimatorLayers animatorLayer)
        {
            return animator.GetLayerWeight((int)animatorLayer);
        }

        public static void SetLayerWeight(this Animator animator, AnimatorLayers animatorLayer, float weight)
        {
            animator.SetLayerWeight((int)animatorLayer, weight);
        }
    }

    public enum AnimatorLayers
    {
        Locomotion = 0,
        Up = 1,
        Down = 2,
    }
}
﻿namespace AnimatorProject.Utils
{
    public static class AnimatorParams
    {
        /// <summary>
        /// Float
        /// </summary>
        public const string Horizontal = "Horizontal";
        /// <summary>
        /// Float
        /// </summary>
        public const string Vertical = "Vertical";
        /// <summary>
        /// Trigger
        /// </summary>
        public const string Jump = "Jump";
        /// <summary>
        /// Bool
        /// </summary>
        public const string Grounded = "Grounded";
        /// <summary>
        /// Bool
        /// </summary>
        public const string Idle = "Idle";
        /// <summary>
        /// Trigger
        /// </summary>
        public const string Attack = "Attack";
        /// <summary>
        /// Int
        /// </summary>
        public const string AttackType = "AttackType";

        /// <summary>
        /// Trigger
        /// </summary>
        public const string Damage = "Damage";

        /// <summary>
        /// Trigger
        /// </summary>
        public const string Death = "Death";
    }
}

﻿using UnityEngine;

namespace AnimatorProject.Utils
{
    public class InputParams : MonoBehaviour
    {
        public const string Horizontal = "Horizontal";
        public const string Vertical = "Vertical";
        public const string MouseX = "Mouse X";
        public const string MouseY = "Mouse Y";
    }
}

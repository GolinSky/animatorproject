﻿using System;
using UnityEngine;

namespace Utils
{
    public static class MathUtils
    {
        public const float MinTolerance = 0.05f;

        public static bool IsEquals(float leftOperand,float rightOperand)
        {
            return Math.Abs(leftOperand - rightOperand) < MinTolerance;
        }

        public static bool IsEquals(Vector3 leftOperand, Vector3 rightOperand)
        {
            return IsEquals(leftOperand.x, rightOperand.x) 
                && IsEquals(leftOperand.y, rightOperand.y)
                && IsEquals(leftOperand.z, rightOperand.z);
        }

        public static bool IsEqualZero(float value, float tolerance)
        {
            return Math.Abs(value) < tolerance;
        }

        public static bool IsEqualZero(float value)
        {
            return IsEqualZero(value, MinTolerance);
        }

        public static bool IsEqualZero(Vector2 moveDirection)
        {
            return IsEqualZero(VectorSum(moveDirection), MinTolerance);
        }

        private static float VectorSum(Vector2 vector2) => Mathf.Abs(vector2.x) + Mathf.Abs(vector2.y);

        /// <summary>
        /// Clamp between -1 to 1
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Vector2 Clamp1(Vector2 direction)
        {
            direction.x = Mathf.Clamp(direction.x, -1, 1);
            direction.y = Mathf.Clamp(direction.y, -1, 1);
            return direction;
        }

        public static bool NotZero(Vector2 keyBoardDirection) => !IsEqualZero(VectorSum(keyBoardDirection), MinTolerance);

    }
}

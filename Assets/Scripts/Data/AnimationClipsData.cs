﻿using System.Collections.Generic;
using System.Linq;
using AnimatorProject.Controllers;
using UnityEngine;

namespace AnimatorProject.Data
{
    [CreateAssetMenu(fileName = "AnimationClipsData", menuName = "Library/Actor Data/Animation Clips Data", order = 4)]
    public class AnimationClipsData : ScriptableObject, IStorable
    {
        [SerializeField] private List<AttackAnimationClipPreset> attackAnimationClipPresets;
        private Dictionary<AttackType, float> lengthAttackDictionary;

        public void Init()
        {
            lengthAttackDictionary = new Dictionary<AttackType, float>(attackAnimationClipPresets.Count);
            foreach (var preset in attackAnimationClipPresets)
            {
                preset.Init();
                lengthAttackDictionary.Add(preset.AttackType, preset.Length);
            }
        }

        public float GetLength(AttackType attackType)
        {
            if (lengthAttackDictionary.ContainsKey(attackType))
            {
                return lengthAttackDictionary[attackType];
            }
            else
            {
                Debug.LogWarning($"There is no animation clips with current type: {attackType} in AnimationClipsData");
                return lengthAttackDictionary.FirstOrDefault().Value;
            }
        }
    }
}
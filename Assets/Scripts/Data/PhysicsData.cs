﻿using UnityEngine;

namespace AnimatorProject.Data
{
    [CreateAssetMenu(fileName = "PhysicData", menuName = "Library/Physic Data", order = 2)]

    public class PhysicsData : ScriptableObject, IStorable
    {
        [SerializeField] private LayerMask groundLayerMask;
        [SerializeField] private LayerMask collisionLayerMask;

        private int groundLayerMaskValue;

        public LayerMask CollisionLayerMask => collisionLayerMask;
        public int GroundLayerMaskValue => groundLayerMaskValue;

        public void Init()
        {
            groundLayerMaskValue = groundLayerMask.value;
        }
    }
}

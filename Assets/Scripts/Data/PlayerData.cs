﻿using UnityEngine;

namespace AnimatorProject.Data
{
    [CreateAssetMenu(fileName = "PlayerData", menuName = "Library/Actor Data/Player Data", order = 1)]
    public class PlayerData : ScriptableObject, IStorable
    {
        public MovementData movementData;
        public AnimatorIkData animatorIkData;
        public AnimationClipsData animationClipsLibrary;

        public void Init()
        {
            movementData.Init();
            animatorIkData.Init();
            animationClipsLibrary.Init();
        }
    }
}

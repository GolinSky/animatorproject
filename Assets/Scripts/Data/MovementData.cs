﻿using System.Collections.Generic;
using System.Linq;
using AnimatorProject.Controllers.Movement;
using UnityEngine;

namespace AnimatorProject.Data
{
    [CreateAssetMenu(fileName = "MovementData", menuName = "Library/Actor Data/Movement Data", order = 2)]
    public class MovementData : ScriptableObject, IStorable
    {
        [SerializeField] private List<SpeedModifierPreset> speedModifierPresets = new List<SpeedModifierPreset>();
        [SerializeField] private Dictionary<SpeedModifierType, SpeedModifier> speedModifierDictionary;
        [SerializeField] private VelocityModifier velocityModifier;
        [SerializeField] private float jumpDuration = 1f;
        [SerializeField] private float jumpDelay = 1f;
        [SerializeField] private float rotateSpeed = 10f;

        public float VelocityAccelaration => velocityModifier.Acceleration;
        public float JumpDuration => jumpDuration;
        public float JumpDelay => jumpDuration;
        public float RotateSpeed => rotateSpeed;

        public void Init()
        {
            speedModifierDictionary = new Dictionary<SpeedModifierType, SpeedModifier>(speedModifierPresets.Count);

            foreach (var preset in speedModifierPresets)
            {
                speedModifierDictionary.Add(preset.SpeedModifierType, preset.SpeedModifier);
            }
        }

        public SpeedModifier FindSpeedModifier(SpeedModifierType speedModifierType)
        {
            //todo: check dictionary cache elements logic!
            if (speedModifierDictionary.ContainsKey(speedModifierType))
            {
                return speedModifierDictionary[speedModifierType];
            }
            Debug.LogWarning($"There is no Speed Modifier Type found in collection on { name }");
            return speedModifierDictionary.FirstOrDefault().Value;
        }

    }
}

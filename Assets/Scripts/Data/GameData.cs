﻿using System;
using UnityEngine;

namespace AnimatorProject.Data
{
    [Serializable]
    public class GameData: IStorable
    {
        public PlayerData playerData;
        public PhysicsData physicsData;

        public void Init()
        {
            playerData.Init();
            physicsData.Init();
        }
    }
}

﻿using System;
using AnimatorProject.Controllers;
using UnityEngine;

namespace AnimatorProject.Data
{
    [Serializable]
    public class AttackAnimationClipPreset
    {
        [SerializeField] private AttackType attackType;
        [SerializeField] private AnimationClip animationClip;
        private float animationLength;

        public void Init()
        {
            animationLength = animationClip.length; //cache length
        }

        public AttackType AttackType => attackType;
        public float Length => animationLength;
    }
}
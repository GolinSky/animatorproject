﻿using System.Collections;
using AnimatorProject.Utils;
using UnityEngine;
using Utils;

namespace AnimatorProject.Data
{
    [CreateAssetMenu(fileName = "AnimatorIkData", menuName = "Library/Actor Data/Animator Ik Data", order = 3)]
    public class AnimatorIkData : ScriptableObject, IStorable
    {
        [SerializeField] private FootIkData rightFootData;
        [SerializeField] private FootIkData leftFootData;
        [SerializeField] private BodyIkData bodyIkData;

        public FootIkData RightFootData => rightFootData;
        public FootIkData LeftFootData => leftFootData;

        public void Init() { }

        public void Init(ActorIkData actorIkData, Animator animator)
        {
            rightFootData.Init(animator, actorIkData.RightFootTransform);
            leftFootData.Init(animator, actorIkData.LeftFootTransform);
            bodyIkData.Init(animator, actorIkData.LookTarget, actorIkData);
        }

        public void ResetFootIkGoal()
        {
            rightFootData.ResetIkWeight();
            leftFootData.ResetIkWeight();
        }

        public void SetBodyWeights() => bodyIkData.SetWeight();
        public void SetHandsIk(float weights) => bodyIkData.SetHandIk(weights);
    }
}

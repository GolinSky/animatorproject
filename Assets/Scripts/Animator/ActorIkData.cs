﻿using System;
using UnityEngine;

namespace AnimatorProject.Utils
{
    [Serializable]
    public struct ActorIkData
    {
        [SerializeField] private Transform rightFootTransform;
        [SerializeField] private Transform leftFootTransform;
        [SerializeField] private Transform lookTarget;

        public Transform RightFootTransform => rightFootTransform;
        public Transform LeftFootTransform => leftFootTransform;
        public Transform LookTarget => lookTarget;
    }
}
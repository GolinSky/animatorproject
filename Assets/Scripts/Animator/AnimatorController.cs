﻿using AnimatorProject.Animation.Modules;
using AnimatorProject.Controllers.Actor;
using AnimatorProject.Utils;
using UnityEngine;

namespace AnimatorProject.Controllers
{
    [RequireComponent(typeof(ActorController))]
    [RequireComponent(typeof(Animator))]
    public class AnimatorController : BaseController, IMachineState
    {
        [SerializeField][HideInInspector] private ActorController actorController;
        [SerializeField][HideInInspector] private Animator animator;

        [SerializeField] private MovementAnimatorModule movementAnimatorModule;
        [SerializeField] private AirAnimatorModule airAnimatorModule;
        [SerializeField] private AttackAnimatorModule attackAnimatorModule;
        [SerializeField] private AdditiveAnimationModule additiveAnimationModule;

        private Vector2 direction;
        private bool isGrounded = true;

        public Animator Animator => animator;

        private void OnValidate()
        {
            actorController = GetComponent<ActorController>();
            animator = GetComponent<Animator>();
        }

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            movementAnimatorModule.Init(this);
            airAnimatorModule.Init(this);
            attackAnimatorModule.Init(this);
            additiveAnimationModule.Init(this);
        }

        protected override void AddListeners()
        {
            actorController.MovementController.JumpStarted += airAnimatorModule.SetJump;
            actorController.MovementController.OnMoved += movementAnimatorModule.SetMovedState;
            actorController.MovementController.OnMoved += attackAnimatorModule.UpdateUpLayer;
            actorController.GravityController.Grounded += airAnimatorModule.CheckGroundState;
            actorController.AttackController.OnAttacked += attackAnimatorModule.SetAttack;
            actorController.OnUpdate += SetUpMovement;
            actorController.GravityController.Grounded += UpdateGroundedState;
            actorController.HealthController.OnChangeHealth += additiveAnimationModule.TakeDamage;
        }

        protected override void RemoveListeners()
        {
            actorController.MovementController.JumpStarted -= airAnimatorModule.SetJump;
            actorController.MovementController.OnMoved -= movementAnimatorModule.SetMovedState;
            actorController.MovementController.OnMoved -= attackAnimatorModule.UpdateUpLayer;
            actorController.GravityController.Grounded -= airAnimatorModule.CheckGroundState;
            actorController.AttackController.OnAttacked -= attackAnimatorModule.SetAttack;
            actorController.OnUpdate -= SetUpMovement;
            actorController.GravityController.Grounded -= UpdateGroundedState;
            actorController.HealthController.OnChangeHealth -= additiveAnimationModule.TakeDamage;
        }

        private void UpdateGroundedState(bool isGrounded) => this.isGrounded = isGrounded;

        private void SetUpMovement(InputController inputController)
        {
            if(!isGrounded) return;
            
            movementAnimatorModule.SetMovement(inputController.KeyBoardDirection);
        }

        public void Activate()
        {
            AddListeners();
        }

        public void Deactivate()
        {
            animator.SetTrigger(AnimatorParams.Death);
            RemoveListeners();
        }
    }
}

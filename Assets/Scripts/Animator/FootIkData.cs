﻿using System;
using UnityEngine;

namespace AnimatorProject.Utils
{
    [Serializable]
    public class FootIkData
    {
        [SerializeField] AvatarIKGoal avatarIkGoal;
        [Range(0f, 1f)] [SerializeField] private float weightPosition;
        [Range(0f, 1f)] [SerializeField] private float weightRotation;

        private Transform footTransform;
        private Transform animatorTransform;
        private Animator animator;
        private Quaternion footRotation;
        private Vector3 targetFootPosition;
        private float currentFootWeight;

        public Vector3 FootPosition => footTransform.position;

        public void Init(Animator animator, Transform footTransform)
        {
            this.animator = animator;
            this.footTransform = footTransform;
            targetFootPosition = footTransform.position;
            animatorTransform = animator.transform;
        }

        public void SetFootWeight(float speedIk)
        {
            if (!Mathf.Approximately(currentFootWeight, weightPosition))
            {
                currentFootWeight = Mathf.Lerp(currentFootWeight, weightPosition, Time.deltaTime * speedIk);
            }
            animator.SetIKPositionWeight(avatarIkGoal, currentFootWeight);
            animator.SetIKRotationWeight(avatarIkGoal, currentFootWeight); //todo:change
        }

        public void ResetIkWeight() => currentFootWeight = 0f;
        public void ResetIkWeight(float value) => currentFootWeight = value;

        public void UpdateIkWeights(RaycastHit hit, float speedIk, Vector3 offset)
        {
            SetFootWeight(speedIk);
            CalculateTargetPosition(hit.point + offset, speedIk);
            footRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(animatorTransform.forward, hit.normal), hit.normal);
            CalculateTargetRotation(footRotation);
        }

        private void CalculateTargetPosition(Vector3 targetPoint, float speed)
        {
            targetFootPosition = targetPoint;
            animator.SetIKPosition(avatarIkGoal, targetFootPosition);
        }

        private void CalculateTargetRotation(Quaternion footRotation)
        {
            if(weightPosition > 0)
            {
                animator.SetIKRotation(avatarIkGoal, footRotation);
            }
        }
    }
}
﻿using System;
using UnityEngine;
using Utils;

namespace AnimatorProject.Utils
{
    [Serializable]
    public struct BodyIkData
    {
        [Range(0, 1)] [SerializeField] private float weight;
        [Range(0, 1)] [SerializeField] private float bodyWeight;
        [Range(0, 1)] [SerializeField] private float headWeight;
        [Range(0, 1)] [SerializeField] private float eyesWeight;
        [Range(0, 1)] [SerializeField] private float clampWeight;
        [SerializeField] private float bodyLerpSpeed;

        [Header("Hand Ik")]
        [Space]
        [Range(0, 1)] [SerializeField] private float rightHandIkWeight;
        [Range(0, 1)] [SerializeField] private float leftHandIkWeight;
        [SerializeField] private float handLerpSpeed;

        private Transform lookTarget;
        private ActorIkData actorIkData;
        private Animator animator;
        private Vector3 originLookTargetPosition;
        private Vector3 currentLookPosition;
        private Vector3 cachedLookPosition;
        private Quaternion cachedLookRotation;

        private float currentHandIkWeight;

        public void Init(Animator animator, Transform lookTarget, ActorIkData actorIkData)
        {
            this.animator = animator;
            this.lookTarget = lookTarget;
            originLookTargetPosition = lookTarget.position;
            currentLookPosition = originLookTargetPosition;
            this.actorIkData = actorIkData;
            currentHandIkWeight = 0f;
        }

        public void SetWeight()
        {
            animator.SetLookAtWeight(weight, bodyWeight, headWeight, eyesWeight, clampWeight);

            if (lookTarget != null)
            {
                cachedLookPosition = lookTarget.position;
                cachedLookRotation = lookTarget.rotation;

                currentLookPosition = MathUtils.IsEquals(currentLookPosition, cachedLookPosition)
                    ? cachedLookPosition
                    : Vector3.Lerp(currentLookPosition, cachedLookPosition, Time.deltaTime * bodyLerpSpeed);
                animator.SetLookAtPosition(currentLookPosition);

                animator.SetIKPosition(AvatarIKGoal.RightHand, cachedLookPosition);
                animator.SetIKRotation(AvatarIKGoal.RightHand, cachedLookRotation);

                animator.SetIKPosition(AvatarIKGoal.LeftHand, cachedLookPosition);
                animator.SetIKRotation(AvatarIKGoal.LeftHand, cachedLookRotation);

                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, currentHandIkWeight);
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, currentHandIkWeight);

            }
        }

        //public void ResetWeights()
        //{
        //    if (lookTarget != null)
        //    {
        //        lookTarget.position = originLookTargetPosition;
        //    }
        //}

        public void SetHandIk(float targetWeight) => currentHandIkWeight = targetWeight;
    }
}
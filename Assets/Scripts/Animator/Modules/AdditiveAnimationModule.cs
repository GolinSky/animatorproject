﻿using System;
using AnimatorProject.Utils;
using UnityEngine;

namespace AnimatorProject.Animation.Modules
{
    [Serializable]
    public class AdditiveAnimationModule : BaseAnimatorModule
    {
        [Tooltip("Delay Between damage clips")]
        [Range(0.1f, 2f)][SerializeField] private float delay;
        private float nextTimeDamage;

        public void TakeDamage(float value)
        {
            if(nextTimeDamage > Time.time ) return;

            nextTimeDamage = Time.time + delay;

            //todo: check value for big/small damage clips
            animator.SetTrigger(AnimatorParams.Damage);
        }
    }
}

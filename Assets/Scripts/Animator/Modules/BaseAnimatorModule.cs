﻿
using System.Collections;
using AnimatorProject.Controllers;
using UnityEngine;

namespace AnimatorProject.Animation.Modules
{
    public abstract class BaseAnimatorModule
    {
        protected Animator animator;
        private MonoBehaviour monoBehaviour;

        public virtual void Init(AnimatorController animatorController)
        {
            this.animator = animatorController.Animator;
            monoBehaviour = animatorController;
        }

        public virtual void StartCoroutine(ref Coroutine coroutine, IEnumerator enumerator)
        {
            if (coroutine != null)
            {
                monoBehaviour.StopCoroutine(coroutine);
            }
            coroutine = monoBehaviour.StartCoroutine(enumerator);
        }

        ~BaseAnimatorModule()
        {
            monoBehaviour?.StopAllCoroutines();
        }
    }
}

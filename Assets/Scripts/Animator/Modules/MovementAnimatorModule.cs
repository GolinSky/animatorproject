﻿using System;
using AnimatorProject.Utils;
using UnityEngine;
using Utils;

namespace AnimatorProject.Animation.Modules
{
    [Serializable]
    public class MovementAnimatorModule: BaseAnimatorModule
    {
        [SerializeField] protected float speedBlend;
        [SerializeField] protected float zeroBlend;
        [SerializeField] protected Vector2 direction;


        public void SetMovement(Vector2 direction)
        {
            if (MathUtils.IsEqualZero(direction))
            {
                this.direction = MathUtils.IsEqualZero(this.direction)
                    ? Vector2.zero
                    : Vector2.Lerp(this.direction, Vector2.zero, zeroBlend * Time.deltaTime);
            }
            else
            {
                this.direction = Vector2.Lerp(this.direction, direction, speedBlend * Time.deltaTime);
                this.direction = MathUtils.Clamp1(this.direction);
            }

            animator.SetFloat(AnimatorParams.Horizontal, this.direction.x);
            animator.SetFloat(AnimatorParams.Vertical, this.direction.y);
        }

        public void SetMovedState(bool isMoved) => animator.SetBool(AnimatorParams.Idle, !isMoved);

    }
}

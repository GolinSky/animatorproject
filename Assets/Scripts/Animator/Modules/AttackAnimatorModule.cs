﻿using System;
using System.Collections;
using AnimatorProject.Controllers;
using AnimatorProject.Utils;
using UnityEngine;
using Utils;

namespace AnimatorProject.Animation.Modules
{
    [Serializable]
    public class AttackAnimatorModule: BaseAnimatorModule
    {
        [Range(0, 1)] [SerializeField] private float maxUpLayerWeight = 1;
        [Range(0, 1)] [SerializeField] private float minUpLayerWeight = 0;

        [Tooltip("Layer, which must be disabled while moving")]
        [SerializeField] private AnimatorLayers targetLayer;
        [SerializeField] private float speedLerp;

        private Coroutine layerUpdateCoroutine;

        //todo: another int param for determing attack type
        public void SetAttack(AttackType attackType)
        {
            animator.SetInteger(AnimatorParams.AttackType, (int)attackType);
            animator.SetTrigger(AnimatorParams.Attack);
        }

        public void UpdateUpLayer(bool isMoved)
        {
            float targetWeight = isMoved ? minUpLayerWeight : maxUpLayerWeight;
            StartCoroutine(ref layerUpdateCoroutine, UpdateLayerCoroutine(targetWeight));
        }

        private void SetLayerWeight(AnimatorLayers animatorLayers, float targetWeight)
        {
            animator.SetLayerWeight(animatorLayers, targetWeight);
        }

        private IEnumerator UpdateLayerCoroutine(float targetWeight)
        {
            float currentWeight = animator.GetLayerWeight(targetLayer);
            while (!MathUtils.IsEquals(currentWeight, targetWeight))
            {
                yield return new WaitForEndOfFrame();
                currentWeight = Mathf.Lerp(currentWeight, targetWeight, speedLerp * Time.deltaTime);
                SetLayerWeight(targetLayer, currentWeight);
            }
            SetLayerWeight(targetLayer, targetWeight);

            layerUpdateCoroutine = null;
        }
    }
}

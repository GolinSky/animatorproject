﻿using System;
using AnimatorProject.Utils;

namespace AnimatorProject.Animation.Modules
{
    [Serializable]
    public class AirAnimatorModule : BaseAnimatorModule
    {
        public void CheckGroundState(bool grounded) => SetGrounded(grounded);
        public void SetJump() => animator.SetTrigger(AnimatorParams.Jump);
        public void SetGrounded(bool state) => animator.SetBool(AnimatorParams.Grounded, state);
    }
}
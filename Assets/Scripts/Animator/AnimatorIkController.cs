﻿using System.Collections;
using AnimatorProject.Controllers;
using AnimatorProject.Controllers.Actor;
using AnimatorProject.Data;
using AnimatorProject.Managers;
using UnityEngine;
using Utils;

namespace AnimatorProject.Utils
{
    [RequireComponent(typeof(ActorController))]
    public class AnimatorIkController : BaseController, IMachineState
    {
        [SerializeField] private ActorIkData actorIkData;
        [Tooltip("Offset of origin cast point")]
        [SerializeField] private Vector3 offset;

        [SerializeField] private float speedIk = 1f;
        [SerializeField] private float maxDistance = 1.2f;
        [Range(0,1)][SerializeField] private float handsWeight;

        [HideInInspector] [SerializeField] private ActorController actorController;


        private readonly Vector3 rayOffset = Vector3.up;
        private readonly Vector3 rayDirection = Vector3.down;
        private PhysicsData physicsData;
        private AnimationClipsData animationClipsData;
        private AnimatorIkData animatorIkData;
        private Coroutine handsIkCoroutine;
        private Animator animator;
        private RaycastHit hit;

        private void OnValidate()
        {
            actorController = GetComponent<ActorController>();
        }

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            animator = actorController.AnimatorController.Animator;

            var gameData = GameManager.instance.gameData;

            animatorIkData = gameData.playerData.animatorIkData;
            animatorIkData.Init(actorIkData, animator);

            physicsData = gameData.physicsData;
            animationClipsData = gameData.playerData.animationClipsLibrary;
        }

        protected override void AddListeners()
        {
            actorController.AttackController.OnAttacked += SetHandsIk;
        }

        protected override void RemoveListeners()
        {
            actorController.AttackController.OnAttacked -= SetHandsIk;
        }

        private void SetHandsIk(AttackType attackType)
        {
            if(handsIkCoroutine != null) StopCoroutine(handsIkCoroutine);
            handsIkCoroutine = StartCoroutine(SetHandsWeight(animationClipsData.GetLength(attackType)));
        }

        private IEnumerator SetHandsWeight(float duration)
        {
            float finishTime = Time.time + duration;
            while (finishTime > Time.time)
            {
                yield return new WaitForEndOfFrame();
                animatorIkData.SetHandsIk(handsWeight);
            }
        }

        private void OnAnimatorIK(int layerIndex)
        {
            animatorIkData.SetBodyWeights();

            if (actorController.MovementController.InMove)
            {
                animatorIkData.ResetFootIkGoal();
            }
            else
            {
                SetIk(animatorIkData.LeftFootData);
                SetIk(animatorIkData.RightFootData);
            }
        }

        private void SetIk(FootIkData footIkData)
        {
            if (Physics.Raycast(footIkData.FootPosition + rayOffset, rayDirection, out hit, maxDistance, physicsData.GroundLayerMaskValue))
            {
                footIkData.UpdateIkWeights(hit, speedIk, offset);
            }
            else
            {
                animatorIkData.ResetFootIkGoal();
            }
        }

        public void Activate()
        {
            AddListeners();
        }

        public void Deactivate()
        {
            RemoveListeners();
        }
    }
}
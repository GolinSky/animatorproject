﻿using System;
using UnityEngine.SceneManagement;

namespace AnimatorProject.Managers
{
    [Serializable]
    public class LevelManager 
    {
        public void RestartLevel()
        {
            SceneManager.LoadScene(0);
        }
    }
}
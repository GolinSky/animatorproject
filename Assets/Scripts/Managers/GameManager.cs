﻿using System;
using AnimatorProject.Controllers;
using AnimatorProject.Data;
using AnimatorProject.Views;
using UnityEngine;

namespace AnimatorProject.Managers
{
    [RequireComponent(typeof(DebugManager))]
    [RequireComponent(typeof(CursorManager))]

    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;
        public GameData gameData;
        public UiManager uiManager;
        public DebugManager debugManager;
        public LevelManager levelManager;
        public CursorManager cursorManager;
        public ActorManager actorManager;

        private void OnValidate()
        {
            debugManager = GetComponent<DebugManager>();
        }

        private void Awake()
        {
            InitSingleton();
            DontDestroyOnLoad(gameObject);
            Init();
        }

        private void Init()
        {
            gameData.Init();
            cursorManager.Activate();
        }

        private void InitSingleton()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Debug.LogError("Detect an attempt of creation the second instance of GameManager!");
                Destroy(gameObject);
            }
        }

       
    }
}

﻿using UnityEngine;

namespace AnimatorProject.Managers
{
    public class DebugManager : MonoBehaviour
    {
        private void ManipulateCursor() //todo: make class for this
        {
            //todo: use ifUnityEditor
            if (Input.GetKeyDown(KeyCode.F12))
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }

            if (Input.GetKeyDown(KeyCode.F11))
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }

        private void LateUpdate()
        {
            ManipulateCursor();
        }
    }
}

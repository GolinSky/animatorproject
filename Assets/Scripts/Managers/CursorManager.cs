﻿using System;
using AnimatorProject.Controllers;
using UnityEngine;

namespace AnimatorProject.Managers
{
    [Serializable]
    public class CursorManager
    {
        public void Activate()
        {
            AddListeners();
        }

        public void Deactivate()
        {
            RemoveListeners();
        }

        private void AddListeners()
        {
            GameManager.instance.actorManager.OnActorRegistered += LockCursor;
            GameManager.instance.actorManager.OnActorDestroy += UnLockCursor;
        }

        private void RemoveListeners()
        {
            GameManager.instance.actorManager.OnActorRegistered -= LockCursor;
            GameManager.instance.actorManager.OnActorDestroy -= UnLockCursor;
        }

        private void UnLockCursor()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        private void LockCursor(ActorController actorController)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
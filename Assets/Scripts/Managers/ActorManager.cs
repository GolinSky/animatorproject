﻿using System;
using AnimatorProject.Controllers;

namespace AnimatorProject.Managers
{
    [Serializable]
    public class ActorManager
    {
        public event Action<ActorController> OnActorRegistered;
        public event Action OnActorDestroy;

        private ActorController actorController;

        public ActorController ActorController => actorController;

        public void RegisterActorController(ActorController actorController)
        {
            this.actorController = actorController;
            OnActorRegistered?.Invoke(actorController);
        }

        public void UnregisterActorController()
        {
            OnActorDestroy?.Invoke();
        }
    }
}
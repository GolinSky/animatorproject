﻿using AnimatorProject.Data;
using AnimatorProject.Managers;
using UnityEngine;

namespace AnimatorProject.Controllers.Collision
{
    public abstract class BaseCollision : MonoBehaviour
    {
        [SerializeField] protected CollisionType collisionType;
        [SerializeField] protected float damage;

        private PhysicsData physicsData;
        protected LayerMask collisionLayerMask;
        protected Transform cachedTransform;

        protected virtual void Start()
        {
            cachedTransform = transform;
            physicsData = GameManager.instance.gameData.physicsData;
            collisionLayerMask = physicsData.CollisionLayerMask;
        }

        protected void TrySetDamage(Collider collider)
        {
            IDamagable damagable = collider.GetComponent<IDamagable>();

            if (damagable != null)
            {
                damagable.GetDamage(damage);
            }
        }      
    }
}
﻿using UnityEngine;

namespace AnimatorProject.Controllers.Collision
{
    public sealed class FireCollision : BaseCollision
    {
        [SerializeField] private float castRadius;
        private RaycastHit raycastHit;
        private Collider[] hitColliders = new Collider[10];
        private int hitCount = 0;

        private void FixedUpdate()
        {
            hitCount = Physics.OverlapSphereNonAlloc(cachedTransform.position, castRadius, hitColliders, collisionLayerMask);
            TryHitTargets();
        }

        private void TryHitTargets()
        {
            for (int i = 0; i < hitCount; i++)
            {
                TrySetDamage(hitColliders[i]);
            }
        }
    }
}

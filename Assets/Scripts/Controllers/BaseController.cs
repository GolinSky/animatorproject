﻿using UnityEngine;

namespace AnimatorProject.Controllers
{
    public abstract class BaseController : MonoBehaviour
    {
        protected abstract void AddListeners();
        protected abstract void RemoveListeners();

        protected virtual void OnDestroy()
        {
            RemoveListeners();
        }
    }
}

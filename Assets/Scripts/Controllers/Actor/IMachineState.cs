﻿namespace AnimatorProject.Controllers.Actor
{
    public interface IMachineState
    {
        void Activate();
        void Deactivate();
    }
}

﻿
namespace AnimatorProject.Controllers
{
    public interface IDamagable
    {
        void GetDamage(float value);
    }
}

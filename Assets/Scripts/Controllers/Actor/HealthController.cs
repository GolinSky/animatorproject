﻿using System;
using AnimatorProject.Controllers.Actor;
using AnimatorProject.Views;
using UnityEngine;

namespace AnimatorProject.Controllers
{
    public class HealthController : BaseController, IDamagable, IMachineState
    {
        public event Action OnDied;
        public event Action<float> OnChangeHealth;

        [SerializeField] private float healthValue;
        private ActorView actorView;
        public float MaxHp => healthValue;

        public void GetDamage(float value)
        {
            if (healthValue < value)
            {
                Die();
                return;
            }
            healthValue -= value;
            OnChangeHealth?.Invoke(healthValue);
        }

        private void Die()
        {
            OnDied?.Invoke();
        }

        protected override void AddListeners()
        {
            
        }

        protected override void RemoveListeners()
        {

        }

        public void Activate()
        {
            AddListeners();
        }

        public void Deactivate()
        {
            RemoveListeners();
        }
    }
}

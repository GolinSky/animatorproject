﻿using System;
using AnimatorProject.Controllers.Actor;
using AnimatorProject.Managers;
using UnityEngine;
using AnimatorProject.Scripts.CustomPhysics;
using AnimatorProject.Utils;

namespace AnimatorProject.Controllers
{
    [RequireComponent(typeof(InputController))]
    [RequireComponent(typeof(AnimatorController))]
    [RequireComponent(typeof(MovementController))]
    [RequireComponent(typeof(GravityController))]
    [RequireComponent(typeof(AttackController))]
    [RequireComponent(typeof(HealthController))]
    [RequireComponent(typeof(AnimatorIkController))]
    public class ActorController : BaseController,IMachineState
    {
        public event Action<InputController> OnUpdate;

        [SerializeField] [HideInInspector] private InputController inputController;
        [SerializeField] [HideInInspector] private AnimatorController animatorController;
        [SerializeField] [HideInInspector] private MovementController movementController;
        [SerializeField] [HideInInspector] private GravityController gravityController;
        [SerializeField] [HideInInspector] private AttackController attackController;
        [SerializeField] [HideInInspector] private HealthController healthController;
        [SerializeField] [HideInInspector] private AnimatorIkController animatorIkController;

        private GameManager gameManager;

        public AnimatorController AnimatorController => animatorController;
        public MovementController MovementController => movementController;
        public GravityController GravityController => gravityController;
        public AttackController AttackController => attackController;
        public HealthController HealthController => healthController;
        public AnimatorIkController AnimatorIkController => animatorIkController;

        private void OnValidate()
        {
            inputController = GetComponent<InputController>();
            animatorController = GetComponent<AnimatorController>();
            movementController = GetComponent<MovementController>();
            gravityController = GetComponent<GravityController>();
            attackController = GetComponent<AttackController>();
            healthController = GetComponent<HealthController>();
            animatorIkController = GetComponent<AnimatorIkController>();
        }

        private void Awake()
        {
            Init();
        }

        private void Init()
        {
            gameManager = GameManager.instance;
            gameManager.actorManager.RegisterActorController(this);
        }

        private void Start()
        {
            Activate();
        }

        protected override void AddListeners()
        {
            healthController.OnDied += Deactivate;
        }

        protected override void RemoveListeners()
        {
            gameManager.actorManager.UnregisterActorController();
            healthController.OnDied -= Deactivate;
        }

        private void Update()
        {
            OnUpdate?.Invoke(inputController);
        }

        public void Activate()
        {
            movementController.Activate();
            animatorController.Activate();
            gravityController.Activate();
            inputController.Activate();
            attackController.Activate();
            healthController.Activate();
            animatorIkController.Activate();
            AddListeners();
        }

        public void Deactivate()
        {
            movementController.Deactivate();
            animatorController.Deactivate();
            gravityController.Deactivate();
            inputController.Deactivate();
            attackController.Deactivate();
            healthController.Deactivate();
            animatorIkController.Deactivate();
            RemoveListeners();
        }
    }
}

﻿using System;
using System.Collections;
using AnimatorProject.Controllers;
using AnimatorProject.Controllers.Actor;
using AnimatorProject.Controllers.Movement;
using AnimatorProject.Data;
using AnimatorProject.Managers;
using AnimatorProject.Utils;
using UnityEngine;
using Utils;

namespace AnimatorProject.Scripts.CustomPhysics
{
    public class GravityController : BaseController, IMachineState
    {
        public event Action<bool> Grounded;

        [SerializeField] private Vector3 offset;
        [SerializeField] private float speedReturn = 10f;
       
        [SerializeField] [HideInInspector] private ActorController actorController;
      
        [SerializeField] private float groundDistance = 3f;
        [SerializeField] private float sphereCastRadius = 2f;

        private readonly Vector3 rayDirection = Vector3.down;

        private PhysicsData physicsData;
        private MovementController movementController;
        private Coroutine returnGravityCoroutine;
        private Transform cachedTransform;
        private RaycastHit rayCastHit;

        private float gravityModifier = 1f;
        private bool isGrounded = true;
        private bool groundedState;
        private int hitsCount;
        public bool IsGrounded => isGrounded;
        private Vector3 OriginCastPoint => cachedTransform.position + offset;

        private void OnValidate()
        {
            actorController = GetComponent<ActorController>();
            cachedTransform = transform;
        }

        private void Start()
        {
            groundedState = isGrounded;
            movementController = actorController.MovementController;
            physicsData = GameManager.instance.gameData.physicsData;
        }

        protected override void AddListeners()
        {
            actorController.MovementController.JumpStarted += DisableGravity;
            actorController.MovementController.JumpEnded += ActivateGravity;
            actorController.OnUpdate += UpdateGravity;
        }

        protected override void RemoveListeners()
        {
            actorController.MovementController.JumpStarted -= DisableGravity;
            actorController.MovementController.JumpEnded -= ActivateGravity;
            actorController.OnUpdate -= UpdateGravity;
        }

        private void UpdateGravity(InputController inputController)
        {
            movementController.Move(rayDirection * gravityModifier, SpeedModifierType.Gravity);
            isGrounded = Physics.SphereCast(
                OriginCastPoint,
                sphereCastRadius,
                rayDirection,
                out rayCastHit,
                groundDistance,
                physicsData.GroundLayerMaskValue);

            if (groundedState != isGrounded)
            {
                groundedState = isGrounded;
                Grounded?.Invoke(isGrounded);
            }
        }

        private void DisableGravity()
        {
            if (!isGrounded) return;
            gravityModifier = 0f;
        }

        private void ActivateGravity()
        {
            if (returnGravityCoroutine == null)
            {
                returnGravityCoroutine = StartCoroutine(ReturnToOrigin(1f));
            }
        }

        private IEnumerator ReturnToOrigin(float originValue)
        {
            while (MathUtils.IsEqualZero(gravityModifier - originValue))
            {
                yield return new WaitForEndOfFrame();
                gravityModifier = Mathf.Lerp(gravityModifier, originValue, Time.deltaTime * speedReturn);
            }

            gravityModifier = 1f;
            returnGravityCoroutine = null;
        }

        public void Activate()
        {
            AddListeners();
        }

        public void Deactivate()
        {
            RemoveListeners();
        }
    }
}

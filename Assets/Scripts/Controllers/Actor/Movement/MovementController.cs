﻿using System;
using System.Collections;
using AnimatorProject.Controllers.Actor;
using AnimatorProject.Controllers.Movement;
using AnimatorProject.Data;
using AnimatorProject.Managers;
using AnimatorProject.Utils;
using AnimatorProject.Scripts.CustomPhysics;
using UnityEngine;

namespace AnimatorProject.Controllers
{
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(ActorController))]
    public class MovementController : BaseController, IMachineState
    {
        public event Action JumpStarted;
        public event Action JumpEnded;
        public event Action<bool> OnMoved;

        public MouseLookController mouseLookController;

        [SerializeField] [HideInInspector] private ActorController actorController;
        [SerializeField] [HideInInspector] private CharacterController characterController;

        private MovementData movementData;
        private GravityController gravityController;
        private Coroutine jumpCoroutine;
        private Transform cachedTransform;
        private Vector2 moveDirection;

        private bool isJumped;
        private bool isMoved;
        public bool InMove => isMoved;

        private void OnValidate()
        {
            actorController = GetComponent<ActorController>();
            characterController = GetComponent<CharacterController>();
        }

        private void Awake()
        {
            cachedTransform = transform;
        }

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            gravityController = actorController.GravityController;
            movementData = GameManager.instance.gameData.playerData.movementData;
        }

        protected override void AddListeners()
        {
            actorController.OnUpdate += SetMove;
        }

        protected override void RemoveListeners()
        {
            actorController.OnUpdate -= SetMove;
        }

        private void SetMove(InputController inputController)
        {
            JumpFromInput();
            SetMoveState(inputController);
            if (isMoved)
            {
                MoveFromInput(inputController.KeyBoardDirection);
            }
            else
            {
                //todo: check each axis for zero condition 
                movementData.FindSpeedModifier(SpeedModifierType.Forward).ResetSpeed();
                movementData.FindSpeedModifier(SpeedModifierType.Side).ResetSpeed();
            }
            RotateFromInput(inputController.MouseDirection.x);
            RotateCameraFromInput(inputController.MouseDirection.y);
        }

        private void SetMoveState(InputController inputController)
        {
            if (isMoved != inputController.IsPressed)
            {
                OnMoved?.Invoke(inputController.IsPressed);
            }

            isMoved = inputController.IsPressed;
        }

        public void Move(Vector3 direction, SpeedModifierType speedModifierType)
        {
            characterController.Move(direction * movementData.FindSpeedModifier(speedModifierType).Speed);
        }

        /// <summary>
        /// Method, which moved a character 
        /// </summary>
        private void MoveFromInput(Vector2 moveDirection)
        {
            this.moveDirection = moveDirection;

            characterController.Move(cachedTransform.forward * moveDirection.y * movementData.FindSpeedModifier(SpeedModifierType.Forward).Speed * movementData.VelocityAccelaration);
            Move(cachedTransform.right * moveDirection.x, SpeedModifierType.Side);
        }      

        private void JumpFromInput()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (!isJumped && gravityController.IsGrounded)
                {
                    if (jumpCoroutine != null)
                    {
                        StopCoroutine(jumpCoroutine);
                    }
                    jumpCoroutine = StartCoroutine(PerformJump());
                    JumpStarted?.Invoke();
                }
            }
        }

        private void RotateFromInput(float mouseX)
        {
            cachedTransform.Rotate(Vector2.up * mouseX * Time.deltaTime * movementData.RotateSpeed);
        }

        private void RotateCameraFromInput(float mouseY)
        {
            mouseLookController.RotateCamera(mouseY);
        }

        private IEnumerator PerformJump()
        {
            isJumped = true;
            float endTime = Time.time + movementData.JumpDuration;
            while (endTime > Time.time)
            {
                yield return new WaitForEndOfFrame();
                Move(Vector3.up, SpeedModifierType.Jump);
            }
            JumpEnded?.Invoke();
            jumpCoroutine = null;
            yield return new WaitForSeconds(movementData.JumpDelay);
            isJumped = false;
        }


        public void Activate()
        {
            AddListeners();
        }

        public void Deactivate()
        {
            RemoveListeners();
        }
    }

}

﻿namespace AnimatorProject.Controllers.Movement
{
    public enum SpeedModifierType
    {
        Forward = 0,
        Side = 1,
        Gravity = 2,
        Jump = 3,
    }
}
﻿using System;
using UnityEngine;

namespace AnimatorProject.Controllers.Movement
{
    [Serializable]
    public class VelocityModifier
    {
        private const float defaultAccelaration = 1f;
        [SerializeField] private float acceleration;
        [SerializeField] private KeyCode activateKeyCode;

        public float Acceleration => Input.GetKey(activateKeyCode) ? acceleration : defaultAccelaration;
    }
}
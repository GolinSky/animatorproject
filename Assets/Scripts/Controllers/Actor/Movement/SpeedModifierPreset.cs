﻿using System;
using UnityEngine;

namespace AnimatorProject.Controllers.Movement
{
    [Serializable]
    public class SpeedModifierPreset
    {
        [SerializeField] private SpeedModifier speedModifier;
        [SerializeField] private SpeedModifierType speedModifierType = default;

        public SpeedModifierType SpeedModifierType => speedModifierType;
        public SpeedModifier SpeedModifier => speedModifier;
    }
}
﻿using System;
using UnityEngine;
using Utils;

namespace AnimatorProject.Controllers.Movement
{
    [Serializable]
    public class SpeedModifier
    {
        [SerializeField] private float minSpeed;
        [SerializeField] private float maxSpeed;
        [SerializeField] private float lerpSpeed;
        private float currentSpeed;

        public float Speed
        {
            get
            {
                if (!MathUtils.IsEquals(currentSpeed, maxSpeed))
                {
                    currentSpeed = Mathf.Lerp(currentSpeed, maxSpeed, lerpSpeed * Time.deltaTime);
                }
                return currentSpeed * Time.deltaTime;
            }
        }

        public void ResetSpeed()
        {
            currentSpeed = minSpeed;
        }
    }
}
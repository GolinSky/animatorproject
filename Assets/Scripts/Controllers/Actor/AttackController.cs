﻿using System;
using AnimatorProject.Controllers.Actor;
using AnimatorProject.Utils;
using UnityEngine;

namespace AnimatorProject.Controllers
{
    public class AttackController : BaseController, IMachineState
    {
        public event Action<AttackType> OnAttacked;
        [SerializeField] private KeyCode attackKeyCode;
        [SerializeField] private AttackType defaultAttackType;
        [HideInInspector] [SerializeField] private ActorController actorController;

        private void OnValidate()
        {
            actorController = GetComponent<ActorController>();
        }

        private void Attack(InputController inputController)
        {
            if (Input.GetKeyDown(attackKeyCode))
            {
                OnAttacked?.Invoke(defaultAttackType);
            }
        }

        public void Activate()
        {
            AddListeners();
        }

        public void Deactivate()
        {
            RemoveListeners();
        }

        protected override void AddListeners()
        {
            actorController.OnUpdate += Attack;
        }

        protected override void RemoveListeners()
        {
            actorController.OnUpdate -= Attack;
        }
    }
}

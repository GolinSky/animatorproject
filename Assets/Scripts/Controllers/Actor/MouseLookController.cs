﻿using UnityEngine;

namespace AnimatorProject.Controllers
{
    public class MouseLookController : MonoBehaviour
    {
        [SerializeField] private float maxLimitAngle = 35f;
        [SerializeField] private float minLimitAngle = -35;
        [SerializeField] [HideInInspector] private Transform cachedTransform;
        private Transform target;

        private void Awake()
        {
            cachedTransform = transform;
            target = cachedTransform.root;
        }

        public void RotateCamera(float mouseY)
        {
            var mouseX = mouseY;
            var realX = cachedTransform.localRotation.x * Mathf.Rad2Deg;
            if (realX > maxLimitAngle && realX > 0)
            {
                if (mouseX > 0.0F) mouseX = 0;
            }
            if (realX < minLimitAngle && realX < 0)
            {
                if (mouseX < 0.0F) mouseX = 0;
            }

            cachedTransform.RotateAround(target.position, cachedTransform.right, mouseX);
        }
    }
}
